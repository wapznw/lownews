//
//  main.m
//  最牛新闻
//
//  Created by 自娱自乐自逍遥 on 15-1-31.
//  Copyright (c) 2015年 wdj. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
