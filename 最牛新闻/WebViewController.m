//
//  WebViewController.m
//  最牛新闻
//
//  Created by 自娱自乐自逍遥 on 15-2-1.
//  Copyright (c) 2015年 wdj. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()<UIWebViewDelegate,UIScrollViewDelegate>

@property(nonatomic, strong) MBProgressHUD *hud;
@property(nonatomic, strong) UIWebView *webView;
@property(nonatomic, strong) UILabel *label;
@property(nonatomic, strong) UIBarButtonItem *refreshBtn;

- (void)back:(id)sender;

- (void)refreshPage: (id)sender;

@end

@implementation WebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc]initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    [self.navigationItem setLeftBarButtonItem:btn];
    
    _refreshBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshPage:)];
    [self.navigationItem setRightBarButtonItem: _refreshBtn];
    
    _webView = [[UIWebView alloc]
                          initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - self.navigationController.navigationBar.frame.size.height - StatusbarSize)
                          ];
    
    [_webView setBackgroundColor:[UIColor grayColor]];

    [_webView setDelegate:self];
    [_webView setScalesPageToFit:YES];

    [self.view addSubview: _webView];
    
    _label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
    [_label setCenter: CGPointMake(CGRectGetMidX(_webView.bounds), 20)];
    [_label setText:@"下拉刷新"];
    [_label setFont: [UIFont systemFontOfSize:14]];
    [_label setTextAlignment: NSTextAlignmentCenter];
    [_label setTextColor:[UIColor whiteColor]];
    [_label setTag:10];
    
    [_webView insertSubview:_label atIndex:0];
    
    _hud = [[MBProgressHUD alloc]initWithView:self.view];

    [self.view addSubview: _hud];
    
    _webView.scrollView.delegate = self;
    
    [_webView loadRequest: [NSURLRequest requestWithURL:_url]];
}

- (void)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)refreshPage:(id)sender{
    [_webView reload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark webView

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [_hud show:YES];
    [_hud setLabelText: @"玩命加载中..."];
    [_hud setMode: MBProgressHUDModeIndeterminate];
    [_hud hide:YES afterDelay:5];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [_hud hide:YES];
    [webView stringByEvaluatingJavaScriptFromString:@"$('header.head-fixed').remove();$('.detail').css('padding','10px 10px 0 10px');"];

    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    rotationAnimation.duration = 2;
    rotationAnimation.RepeatCount = 1000;//你可以设置到最大的整数值
    rotationAnimation.cumulative = NO;
    rotationAnimation.removedOnCompletion = NO;
    rotationAnimation.fillMode = kCAFillModeForwards;
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [_hud setLabelText: @"加载失败,刷新试试!"];
    [_hud setMode: MBProgressHUDModeText];
    [_hud hide:YES afterDelay:2];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y < -50) {
        _label.text = @"释放刷新";
    }else{
        _label.text = @"下拉刷新";
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (scrollView.contentOffset.y < -50) {
        [_webView reload];
    }
}


@end
